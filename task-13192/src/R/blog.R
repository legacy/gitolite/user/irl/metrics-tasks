# Load required libraries.
require(ggplot2, warn.conflicts = FALSE, quietly = TRUE)
require(scales, warn.conflicts = FALSE, quietly = TRUE)
require(reshape, warn.conflicts = FALSE, quietly = TRUE)
require(splines, warn.conflicts = FALSE, quietly = TRUE)
require(Hmisc, warn.conflicts = FALSE, quietly = TRUE)

# Read .csv files written by Java.
h <- read.csv("out/csv/hidserv-stats.csv", stringsAsFactors = FALSE)

# Create directories for graphs.
dir.create(file.path("out", "graphs", "blog"), showWarnings = FALSE,
  recursive = TRUE)

# Cut off last two days, because stats might be incomplete for those.
h <- h[as.Date(h$stats_end) < max(as.Date(h$stats_end) - 1), ]

# Graph the number of reported stats by day.
h7 <- data.frame(date = as.Date(h$stats_end), reports = 1)
ggplot(h7, aes(x = date)) +
geom_bar(colour = 'lightgray', width = .7, binwidth = 1) +
scale_x_date("") +
scale_y_continuous("")
ggsave("out/graphs/blog/num-reported-stats.png", width = 10, height = 3,
  dpi = 100)

e <- read.csv("out/csv/hidserv-stats-extrapolated.csv",
  stringsAsFactors = FALSE)
e <- melt(e, by = c("date", "type"))
e <- e[e$variable == "wiqm", ]
e <- rbind(e, data.frame(date = NA, type = c("onions", "cells"),
  variable = NA, value = 0))

ggplot(e[e$type == "cells", ], aes(x = as.Date(date), y = value)) +
geom_line() +
scale_x_date(name = "") +
scale_y_continuous(name = "")
ggsave("out/graphs/blog/extrapolated-cells.png", width = 10,
  height = 3, dpi = 100)

ggplot(e[e$type != "cells", ], aes(x = as.Date(date), y = value)) +
geom_line() +
scale_x_date(name = "") +
scale_y_continuous(name = "")
ggsave("out/graphs/blog/extrapolated-onions.png", width = 10,
  height = 3, dpi = 100)

